#!/usr/bin/python

import sys
import re
import boto3
import yaml
import sys
from botocore.exceptions import ClientError


config = yaml.load(open('config/config.yaml'))

print sys.argv[0]

boto3.setup_default_session(profile_name=config['profile'], region_name=config['region'])

resourcestocheckdict = {'VpcPeeringConnectionId': 'pcx', 'RouteTableId': 'rtb', 'SubnetId': 'subnet'}
countinvalidresources = {}

nonexistantroutetables = []
existingroutetables = []
charstostrip = [':', ' ', '"', ',']


def checkresource(resource):

    for char in charstostrip:
        resource = resource.replace(char, "")

    try:
     ec2client = boto3.client('ec2')
     response = ec2client.describe_route_tables(RouteTableIds=[resource])
     existingroutetables.append(resource)
    except ClientError as e:
     if e.response['Error']['Code'] == 'InvalidRouteTableID.NotFound':
         nonexistantroutetables.append(resource)
     else:
         print "Unexpected error: %s" % e


cidripregex = re.compile('(([2][5][0-5]\.)|([2][0-4][0-9]\.)|([0-1]?[0-9]?[0-9]\.)){3}'
                +'(([2][5][0-5])|([2][0-4][0-9])|([0-1]?[0-9]?[0-9]))\/(3[0-2]|2[0-9]|1[0-9])')
betweenquotesregex = re.compile(':(.*?)$')

extradigitscheck = re.compile('\/(\d){3,}')

correctcidrcounter = 0
incorrectcidrcounter = 0

validcounter = 0

for attributeID, attributeValue in resourcestocheckdict.items():
    countinvalidresources["Invalid-" + attributeID] = 0

for linenum, line in enumerate(sys.stdin.readlines()):
    cidrmatch = cidripregex.search(line)
    quotessearch = betweenquotesregex.search(line)
    extradigitmatch = extradigitscheck.search(line)

    for attributeID, attributeValue in resourcestocheckdict.items():
        if attributeID in line and attributeValue not in line and "}" not in line:
            print 'Line {}{} is an invalid {}({})] Value'.format(linenum, quotessearch.group(), attributeID,attributeValue)
            countinvalidresources["Invalid-" + attributeID] += 1

        if attributeID == 'RouteTableId' and quotessearch and attributeID in line:
            checkresource(quotessearch.group())


    if "Cidr" in line and cidrmatch and not extradigitmatch:
        correctcidrcounter += 1
    elif "Cidr" in line and not cidrmatch or "Cidr" in line and extradigitmatch:
        incorrectcidrcounter += 1
        print 'Line {}{} is an invalid CIDR range'.format(linenum, quotessearch.group())

print "\n\n{} Valid CIDR range values present".format(correctcidrcounter)
print "{} invalid CIDR range values present".format(incorrectcidrcounter)
print "{} TOTAL lines which require CIDR ranges present".format(incorrectcidrcounter + correctcidrcounter)

print('\n')

for attributeID, attributeValue in countinvalidresources.items():
    print '{} {} Values present'.format(attributeValue, attributeID)

print('\n')
print "There are {} non existant route tables".format(len(list(set(nonexistantroutetables))))
for rtid in set(nonexistantroutetables):
    print rtid,
print('\n')
print "{} route table(s) already exists ".format(len(list(set(existingroutetables))))
for rtid in set(existingroutetables):
    print rtid,
print('\n')