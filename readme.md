#CF Buddy

Script for detecting invalid values and non existant resources in cloudformation stacks \
prior to deployment to prevent wasted time from inevitable rollbacks because CF doesnt check these things by itself before deployment.

####Useage:

Pipe a template to the script and give it a config file parameter \
(which should contain the AWS profile to use for the resource checks)

e.g. 

sceptre generate-template DEV/EUW1 routes |./cfbuddy.py config/DEV/config.yaml \

or 

cat exampletemplate.txt | ./cfbuddy.py path/to/configfile.yml

